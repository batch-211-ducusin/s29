/*
MongoDB - Query Operators
	Expand Queries in MongoDB using Query Operators

	Overview:
	- Definition
	- Importance

Types of Query Operatoes:
	1. Comparison Query Operators
		a. Greater Than
		b. Greater than or equal to
		c. less than
		d. less than or equal to
		e. not equal to
		f. in
	2. Evaluation Query Operator
		a. Regex
			I. Case sensitive query
			II. case insensitive query
	3. Logical Query Operators
		a. OR
		b. AND

Field Projection
	1. Inclution
		a. returning specific fields in embedded documents
		b. exception to the inclusion rule
		c. slice operator
	2. Exclusion
		a. excluding specific fields in embedded documents




	What does Query Operator Mean?
	- A "Query" is a request for data from a database.
	- An "Operator" is a symbol that represents an action or a process.
	- Putting them together, they mean the things that we can do on our queries using certain operators.




Why do we need to study Query Operators?
	- Knowing Query Operators will enable us to create queries that can do more than what simple operators do. (we can do more than what we do in simple CRUD Operations)
	- For example, in our CRUD Operations discussion, we have discussed findOne using a specific value inside its single or multiple parameters. When we know query operators, we may look for records that are more specific.
*/


//1. COMPARISON QUERY OPERATORS
/*
	Includes:
		- Greater Than
		- Less Than
		- Greater Than or Equal to
		- Less Than or Equal to
		- Not Equal to
		- In


	1.1 GREATER THAN: $gt
	- $gt finds documents that have fields numbers that are greater than a specific value.
	Syntax:
		db.collectionName.find({field: {$gt: value}});

	1.2 GREATER THAN OR EQUAL TO: $gte
	- $gte finds documents that have field number values that are greater than or equal to a specific value.
	Syntax:
		db.collectionName.find({field: {$gte: value}});
	Test:
		db.users.find({age: {$gte: 76}});

	1.3 LESS THAN OPERATOR: $lt
	- $lt finds documents that have field number values less than the specified value.
	Syntax:
		db.collectionName.find({field: {$lt: value}});
	Test:
		db.users.find({age: {$lt: 65}});

	1.4 LESS THAN OR EQUAL TO OPERATOR: $lte
	- $lte finds documents that have field number values that are less than or equl to a specified value.
	Syntax:
		db.collectionName.find({field: {$lte: value}});
	Test:
		db.users.find({age: {$lte: 65}});

	1.5 NOT EQUAL TO OPERATOR: $ne
	- $ne finds documents that have field numbers that are not equal to a specified value.
	Syntax:
		db.collectionName.find({field: {$ne: value}});
	Test:
		db.users.find({age: {$ne: 65}});


	1.6 IN OPERATOR: $in
	- $in finds documents with specific match criteria on one field using different values.
	Syntax:
		db.collectionName.find({field: {$in: [value]}});
	Test:
		db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
		db.users.find({courses: {$in: ["HTML", "React"]}});
	//dito puwede kang mag search ng multiple value at isa man dun at true, i lalabas nya.
*/


//2. EVALUATION QUERY OPERATORS
/*
	- Evaluate operators return data based on evaluations of either individual fields or the entire collection's documents.


	2.1 REGEX OPERATOR: $regex
	- $regex is short for regular expression.
	- they are called regular espressions because they are based on regular languages.
	- $regex is used for matching strings.
	- it allows us to find documents that match a specific string pattern using regular expressions.

	2.1	CASE SENSITIVE QUERY:
	Syntax:
		db.collectionName.find({field: {$regex: "pattern"}});
	Test:
		db.users.find({lastName: {$regex: "A"}});


	2.1.2 CASE INSENSITIVE QUERY:
	- We can run case-insensitive queries by utilizing the "i" option.
	Syntax:
		db.collectionName.find({field: {$regex: "pattern", $options: "$optionValue"}});
	Test:
		db.users.find({lastName: {$regex: "A", $options: "$i"}});
*/


/*
	Mini Activity
	- Find users with the letter "e" in their fist name.
	- make sure to use case insensitive query.
	- screenshot the result from the Robo3T and sent on our batch hangouts.
	- pass the result at 6:50PM
*/


//3. LOGICAL QUERY OPERATORS
/*
	3.1 OR OPERATOR: $or
	- $or finds documents that match a single criteria from multiple provided scearch criteria.
	Syntax:
		db.collectionName.find({$or: [ {fieldA: "valueA"}, {fieldB: "valueB"} ]});
	test:
		db.users.find({$or: [ {firstName: "Neil"}, {age: "25"} ]});
		db.users.find({$or: [ {firstName: "Neil"}, {age: {$gt: 30}} ]});

	3.2 AND OPERATOR: $and
	- $and finds documents matching multiple criteria in a single field.
	Syntax:
		db.collectionName.find({$and: [ {fieldA: "valueA"}, {fieldB: "valueB"} ]});
	test:
		db.users.find({$and: [ {age: {$ne: 82}}, {age: {$ne: 76}} ]});
*/


/*
	Mini Activity
	- Find users with the letter "e" in their firstName and has an age of less than or equal to 30.

	Answer:
	db.users.find({$and: [ {firstName: {$regex: "e", $options: "$i"}}, {age: {$lte: 30}} ]});
*/


//FIELD PROJECTION
/*
	- by default MongoDB returns the whole document, especially, when dealing with complex documents.
	- But sometimes, it is not helpful to view the whole document, especially when dealing with complex documents.
	- To help with readability of the values returned (or sometimes, because of security reasons), we include or exclude some fields.
	- In other words, we project our selected fields.


	1. INCLUSION
	- Allows us to include or add specific fields only when retrieving documents.
	- The value denoted is "1" to indicate that the field is being included.
	- We cannot do exclusion on field that uses inclusion projection
	Syntax:
		db.collectionName.find({criteria}, {field:1})
	Test:
		db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}
		);

	1.1 RETURNING SPECIFIC FIELDS IN EMBEDDED DOCUMENTS
	- The double qoutations are important.
	Test:
		db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}
		);

	1.2 EXEPTION TO THE INCLUSION RULE: Suppressing the ID field.
	- Allows us to exclude the "_id" field when retrieving documents.
	- When using field projection, field inclution and exclusion may not be used at the same time.
	- Excluding the "_id" field is the ONLY exeption to this rule.
	Syntax:
		db.collectionName.find({criteria},{_id:0})
	Test:
		db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1,
				_id: 0
			}
		);

	1.3 SLICE OPERATOR: $slice
	- $slice operator allows us to retrieve only 1 element that matches the search criteria.

	*to demonstrate, let us first insert and view an array.
	db.users.insert({
			namearr: [
				{
					namea: "Juan"
				},
				{
					nameb: "tamad"
				}
			]	
	});

	db.users.find({
		namearr:
		{
			namea: "Juan"
		}
	});

	*now, let us use the slice operator
	db.users.find(
			{ "namearr": 
				{ 
					namea: "Juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		);
*/


/*
	Mini Activity
	- find users with letter "s" on thier first names. (use the "i" option in the $regex operator)
	- Show only the firstName and the lastName fields and hide the _id field.
	- Send the screenshots to our batch hangouts.

	Answer:
	db.users.find(
		{
			firstName: {$regex: "s", $options: "$i"}
		},
		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
	);
*/

/*
	2. EXCLUSION
	- Allows us to exclude or remove specific fields when retrieving documents.
	- The value provided is zero to denote that the field is being included.
	Syntax:
		db.collectionName.find({criteria}, {field: 0});
	test:
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			contact: 0,
			department: 0
		}
	);

	2.1 EXCLUDING or SUPRESING SPECIFIC FIELDS IN EMBEDDED DOCUMENT
	test:
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			"contact.phone": 0
		}
	);
	//dito nawala yung field nya ng phone sa contact.
*/



db.users.find({$or:
		[
			{firstName: {$regex: "s", $options: "$i"}},
			{lastName: {$regex: "d", $options: "$i"}}
		]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

db.users.find({$and: [ {department: "HR"}, {age: {$gte: 70}} ]});

db.users.find({$and: [ 
	{firstName: {$regex: "e", $options: "$i"}}, {age: {$lte: 30}}
]});